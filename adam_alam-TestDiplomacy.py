from unittest import main, TestCase
from Diplomacy import diplomacy_eval, create_dict, diplomacy_print, diplomacy_solve
from io import StringIO


class TestDiplomacy(TestCase):

    # ***********************
    # *        Eval        *
    # ***********************

    def test_eval_1(self):
        inp = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Support B"
        ]
        solution = diplomacy_eval(inp)
        expected = {'A': "[dead]", 'B': 'Madrid', 'C': "London"}
        self.assertTrue(solution == expected)

    def test_eval_2(self):
        inp = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
        ]
        solution = diplomacy_eval(inp)
        expected = {'A': "[dead]", 'B': '[dead]'}
        self.assertTrue(solution == expected)

    def test_eval_3(self):
        inp = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Support B",
            "D Austin Move London",
        ]
        solution = diplomacy_eval(inp)
        expected = {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': '[dead]'}
        self.assertTrue(solution == expected)

    def test_eval_4(self):
        inp = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid",
        ]
        solution = diplomacy_eval(inp)
        expected = {'A': '[dead]', 'B': '[dead]', 'C': '[dead]'}
        self.assertTrue(solution == expected)

    def test_eval_5(self):
        inp = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid",
            "D Paris Support B",
        ]
        solution = diplomacy_eval(inp)
        expected = {'A': '[dead]', 'B': 'Madrid', 'C': '[dead]', 'D': 'Paris'}
        self.assertTrue(solution == expected)

    def test_eval_6(self):
        inp = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid",
            "D Paris Support B",
            "E Austin Support A"
        ]
        solution = diplomacy_eval(inp)
        expected = {'A': '[dead]', 'B': '[dead]',
                    'C': '[dead]', 'D': 'Paris', 'E': "Austin"}
        self.assertTrue(solution == expected)

    def test_eval_7(self):
        inp = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Support B",
            "D Austin Move London",
            "E Tokyo Support D",
            "F Moscow Move Austin",
            "G Paris Hold",
            "H Berlin Move Paris"
        ]
        solution = diplomacy_eval(inp)
        expected = {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': 'London',
                    'F': 'Austin', 'E': 'Tokyo', 'G': '[dead]', 'H': '[dead]'}
        self.assertTrue(solution == expected)

    def test_eval_8(self):
        inp = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Support B",
            "D Austin Move London",
            "E Tokyo Support London",
            "F Moscow Move Austin",
            "G Paris Hold",
            "H Berlin Move Paris"
        ]
        solution = diplomacy_eval(inp)
        expected = -1
        self.assertTrue(solution == expected)

    def test_eval_9(self):
        inp = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Support B",
            "D Austin Move London",
            "E Tokyo Support London",
            "F Moscow Move Milan",
            "G Paris Hold",
            "H Berlin Move Paris"
        ]
        solution = diplomacy_eval(inp)
        expected = -1
        self.assertTrue(solution == expected)

    def test_eval_10(self):
        inp = [
            "A Madrid Wrong",
        ]
        solution = diplomacy_eval(inp)
        expected = -1
        self.assertTrue(solution == expected)

    def test_eval_11(self):
        inp = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Support B",
            "D Austin Move London",
            "E Tokyo Support London",
            "F Moscow Move Milan",
            "G Paris Hold",
            "H Berlin Move Paris"
        ]
        solution = diplomacy_eval(inp)
        expected = -1
        self.assertTrue(solution == expected)

    # ***********************
    # *        Print        *
    # ***********************

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(
            w, {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': '[dead]'})
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {'A': 'Austin', 'B': 'Paris',
                            'C': 'London', 'D': '[dead]'})
        self.assertEqual(
            w.getvalue(), "A Austin\nB Paris\nC London\nD [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {'A': 'Cairo', 'B': 'Lagos',
                            'C': '[dead]', 'D': 'Nairobi'})
        self.assertEqual(
            w.getvalue(), "A Cairo\nB Lagos\nC [dead]\nD Nairobi\n")

    # ***********************
    # *     Create Dict     *
    # ***********************

    def test_create_1(self):
        inp = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Support B"
        ]
        ca, cb, cc = create_dict(inp)
        self.assertEqual(ca, {'army': "A", 'city': "Madrid", 'action': 'Hold'})
        self.assertEqual(
            cb, {'army': "B", 'city': "Barcelona", 'action': 'Move', 'to': "Madrid"})
        self.assertEqual(
            cc, {'army': "C", 'city': "London", 'action': 'Support', 'to': 'B'})

    def test_create_2(self):
        inp = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid",
            "D Paris Support B",
        ]
        ca, cb, cc, cd,  = create_dict(inp)
        self.assertEqual(ca, {'army': "A", 'city': "Madrid", 'action': 'Hold'})
        self.assertEqual(
            cb, {'army': "B", 'city': "Barcelona", 'action': 'Move', 'to': "Madrid"})
        self.assertEqual(cc, {'army': "C", 'city': "London",
                              'action': 'Move', 'to': 'Madrid'})
        self.assertEqual(cd, {'army': "D", 'city': "Paris",
                              'action': 'Support', 'to': 'B'})

    def test_create_3(self):
        inp = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid",
            "D Paris Support B",
            "E Austin Support A"
        ]
        ca, cb, cc, cd, ce = create_dict(inp)
        self.assertEqual(ca, {'army': "A", 'city': "Madrid", 'action': 'Hold'})
        self.assertEqual(
            cb, {'army': "B", 'city': "Barcelona", 'action': 'Move', 'to': "Madrid"})
        self.assertEqual(cc, {'army': "C", 'city': "London",
                              'action': 'Move', 'to': 'Madrid'})
        self.assertEqual(cd, {'army': "D", 'city': "Paris",
                              'action': 'Support', 'to': 'B'})
        self.assertEqual(
            ce, {'army': "E", 'city': "Austin", 'action': 'Support', 'to': 'A'})

    # ***********************
    # *        Solve        *
    # ***********************

    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support Milan\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "-1\n")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


# ----
# # main
# ----

if __name__ == "__main__":
    main()


""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
