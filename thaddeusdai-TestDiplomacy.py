#!/usr/bin/env python3

# -------------------------------
# projects/Diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import army_place, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # ----
    # army_place
    # ----
    def test_army_place(self):
        myList = army_place(["A Madrid Hold", "B London Move Madrid",
                             "C Paris Move London", "D Amsterdam Support B"])
        myList2 = ["A [dead]", "B Madrid", "C London", "D Amsterdam"]
        self.assertEqual(myList, myList2)

    def test_army_place_2(self):
        myList = army_place([])
        myList2 = []
        self.assertEqual(myList, myList2)

    def test_army_place_3(self):
        list1 = army_place(["A Madrid Hold", "B Barcelona Move Madrid",
                            "C London Support B", "D Austin Move London"])
        list2 = ["A [dead]", "B [dead]", "C [dead]", "D [dead]"]
        self.assertEqual(list1, list2)

    def test_army_place_4(self):
        list1 = army_place(["A Madrid Hold", "B Barcelona Move Madrid"])
        list2 = ["A [dead]", "B [dead]"]
        self.assertEqual(list1, list2)

    def test_army_place_5(self):
        list1 = army_place(["A Madrid Hold", "B Barcelona Move Madrid",
                            "C London Support B", "D Austin Move London", "E Houston Support B"])
        list2 = ['A [dead]', 'B Madrid', 'C [dead]', 'D [dead]', 'E Houston']
        self.assertEqual(list1, list2)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B Madrid", "C London", "D Amsterdam"])
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\nD Amsterdam\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [])
        self.assertEqual(w.getvalue(), "")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B [dead]", "C [dead]", "D [dead]"])
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB London Move Madrid\nC Paris Move London\nD Amsterdam Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\nD Amsterdam\n")

    def test_solve2(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "")

    def test_solve3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
# ----
# main
# ----


if __name__ == "__main__":
    main()
